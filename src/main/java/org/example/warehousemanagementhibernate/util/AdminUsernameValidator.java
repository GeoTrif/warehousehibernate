package org.example.warehousemanagementhibernate.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Utilitary class used to validate the username from the admin user.
 * 
 * @author GeoTrif
 *
 */
public class AdminUsernameValidator {

	/**
	 * Used to perform validation on the admin username.
	 * 
	 * @param userName
	 *            which is the username to be tested.
	 * @return true if the username passes the validations.
	 */
	public boolean validateAdminUserName(String userName) {
		if (checkIfAdminUsernameHasGoodSintax(userName) && checkIfAdminUsernameInDataBase(userName)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Used to perform syntax validation on the admin username.
	 * 
	 * @param userName
	 *            which is the username to be tested.
	 * @return true if the username passes the validations.
	 */
	private boolean checkIfAdminUsernameHasGoodSintax(String userName) {
		boolean isValid = false;

		if (userName.length() > 3 && userName.length() < 30) {
			for (int i = 0; i < userName.length(); i++) {
				if (Character.isLetterOrDigit(userName.charAt(i))) {
					isValid = true;
				}
			}
		}

		return isValid;
	}

	/**
	 * Used to perform validation if the admin username matches the ones in the
	 * database.
	 * 
	 * @param userName
	 *            which is the username to be tested.
	 * @return true if the username passes the validations.
	 */
	private boolean checkIfAdminUsernameInDataBase(String userName) {
		boolean isInDataBase = false;
		String sql = "select * from admins";

		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				String adminUserName = resultSet.getString("user_name");

				if (userName.equals(adminUserName)) {
					isInDataBase = true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return isInDataBase;
	}
}
