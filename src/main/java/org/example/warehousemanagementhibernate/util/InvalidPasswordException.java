package org.example.warehousemanagementhibernate.util;

/**
 * Utilitary class used to declare a particular exception.
 * 
 * @author GeoTrif
 *
 */
public class InvalidPasswordException extends Exception {
	private String msg;

	public InvalidPasswordException() {

	}

	public InvalidPasswordException(String msg) {
		this.msg = msg;
	}
}
