package org.example.warehousemanagementhibernate.util;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionFactoryUtil {

	private static SessionFactory sessionFactory;

	private SessionFactoryUtil() {
	}

	public static SessionFactory makeSession() {
		if (sessionFactory != null) {
			return sessionFactory;
		}

		try {
			sessionFactory = new Configuration().configure().buildSessionFactory();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		return sessionFactory;
	}

}
