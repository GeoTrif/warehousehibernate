package org.example.warehousemanagementhibernate.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Utilitary class used to validate the username from the customer user.
 * 
 * @author GeoTrif
 *
 */
public class CustomerUsernameValidator {

	/**
	 * Used to perform validation on the customer username.
	 * 
	 * @param userName
	 *            which is the username to be tested.
	 * @return true if the username passes the validations.
	 */
	public boolean validateCustomerUserName(String userName) {
		if (checkIfCustomerUsernameHasGoodSintax(userName) && checkIfCustomerUsernameInDataBase(userName)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Used to perform syntax validation on the customer username.
	 * 
	 * @param userName
	 *            which is the username to be tested.
	 * @return true if the username passes the validations.
	 */
	private boolean checkIfCustomerUsernameHasGoodSintax(String userName) {
		boolean isValid = false;

		if (userName.length() > 3 && userName.length() < 30) {
			for (int i = 0; i < userName.length(); i++) {
				if (Character.isLetterOrDigit(userName.charAt(i))) {
					isValid = true;
				}
			}
		}

		return isValid;
	}

	/**
	 * Used to perform validation if the customer username matches the ones in the
	 * database.
	 * 
	 * @param userName
	 *            which is the username to be tested.
	 * @return true if the username passes the validations.
	 */
	private boolean checkIfCustomerUsernameInDataBase(String userName) {
		boolean isInDataBase = false;
		String sql = "select * from customers";

		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				String customerUserName = resultSet.getString("customer_user_name");

				if (userName.equals(customerUserName)) {
					isInDataBase = true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return isInDataBase;
	}
}
