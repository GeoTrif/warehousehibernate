package org.example.warehousemanagementhibernate.util;

/**
 * Utilitary class used to declare a particular exception.
 * 
 * @author GeoTrif
 *
 */
public class InvalidUsernameException extends Exception {
	private String msg;

	public InvalidUsernameException() {

	}

	public InvalidUsernameException(String msg) {
		this.msg = msg;
	}

}
