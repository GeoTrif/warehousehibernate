package org.example.warehousemanagementhibernate.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Utilitary class used to make connection to the MySql data base.
 * 
 * @author GeoTrif
 *
 */
public class ConnectionUtil {
	private static final String DB_URL = "jdbc:mysql://localhost:3306/warehouse?autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	private static final String USER = "root";
	private static final String PASSWORD = "decebalus";
	private static Connection connection;

	private ConnectionUtil() {

	}

	public static Connection makeConnection() {

		if (connection != null) {
			return connection;
		}

		try {
			connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return connection;
	}

}
