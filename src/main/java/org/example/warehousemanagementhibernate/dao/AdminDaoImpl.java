package org.example.warehousemanagementhibernate.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.example.warehousemanagementhibernate.mapper.AdminMapper;
import org.example.warehousemanagementhibernate.model.Admin;
import org.example.warehousemanagementhibernate.util.SessionFactoryUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

/**
 * Operational class used to make operations on the admins table from the
 * warehouse dataBase.
 * 
 * @author GeoTrif
 *
 */
public class AdminDaoImpl implements AdminDao {
	private AdminMapper adminMapper = new AdminMapper();

	/**
	 * Used to add admin users to the admins table in the warehouse dataBase.
	 * 
	 * @param admin
	 *            which is an object modeled by Admin class.
	 */
	public void addAdmin(Admin admin) {
		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.save(admin);
			transaction.commit();
			session.close();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
		}
	}

	/**
	 * Used to get admin users by admin id from the admins table in the warehouse
	 * dataBase.
	 * 
	 * @param adminId
	 *            which is an integer.
	 */
	public Admin getAdminById(int adminId) {
		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = session.beginTransaction();
		Admin admin = (Admin) session.get(Admin.class, adminId);

		System.out.println(admin);
		transaction.commit();
		session.close();
		return admin;
	}

	/**
	 * Used to get all admins users from the admins table in the warehouse dataBase.
	 * 
	 */
	public List<Admin> getAllAdmins() {
		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = session.beginTransaction();
		List<Admin> admins = session.createQuery("from Admin").list();

		for (Admin admin : admins) {
			System.out.println(admin);
		}

		transaction.commit();
		session.close();
		return admins;
	}

	/**
	 * Used to update admin users from the admins table in the warehouse dataBase.
	 * 
	 * @param admin
	 *            which is an object modeled by Admin class.
	 */
	public void updateAdmin(Admin admin) {
		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.update(admin);
			transaction.commit();
			session.close();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
		}
	}

	/**
	 * Used to delete admin users from the admins table in the warehouse dataBase.
	 * 
	 * @param admin
	 *            which is an object modeled by Admin class.
	 */
	public void deleteAdmin(Admin admin) {
		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.delete(admin);
			transaction.commit();
			session.close();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
		}
	}
}
