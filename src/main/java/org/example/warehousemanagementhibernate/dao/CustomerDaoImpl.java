package org.example.warehousemanagementhibernate.dao;

import java.util.List;
import org.example.warehousemanagementhibernate.mapper.CustomerMapper;
import org.example.warehousemanagementhibernate.model.Customer;
import org.example.warehousemanagementhibernate.util.SessionFactoryUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * Operational class used to make operations on the customers table from the
 * warehouse dataBase.
 * 
 * @author GeoTrif
 *
 */
public class CustomerDaoImpl implements CustomerDao {
	private CustomerMapper customerMapper = new CustomerMapper();

	/**
	 * Used to add customers users to the customers table in the warehouse dataBase.
	 * 
	 * @param customer
	 *            which is an object modeled by Customer class.
	 */
	public void addCustomer(Customer customer) {
		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.save(customer);
			transaction.commit();
			session.close();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
		}
	}

	/**
	 * Used to get customer users by customer id from the customers table in the
	 * warehouse dataBase.
	 * 
	 * @param customerId
	 *            which is an integer.
	 */
	public Customer getCustomerById(int customerId) {
		Customer customer = null;

		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = session.beginTransaction();
		customer = (Customer) session.get(Customer.class, customerId);

		System.out.println(customer);
		transaction.commit();
		session.close();
		return customer;
	}

	/**
	 * Used to get all customers users from the customers table in the warehouse
	 * dataBase.
	 * 
	 */
	public List<Customer> getAllCustomers() {
		List<Customer> customers = null;

		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = session.beginTransaction();
		customers = session.createQuery("from Customer").list();

		for (Customer customer : customers) {
			System.out.println(customer);
		}

		transaction.commit();
		session.close();
		return customers;
	}

	/**
	 * Used to update customers users from the customers table in the warehouse
	 * dataBase.
	 * 
	 * @param customer
	 *            which is an object modeled by Customer class.
	 */
	public void updateCustomer(Customer customer) {
		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.update(customer);
			transaction.commit();
			session.close();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
		}
	}

	/**
	 * Used to delete customer users from the customers table in the warehouse
	 * dataBase.
	 * 
	 * @param customer
	 *            which is an object modeled by Customer class.
	 */
	public void deleteCustomer(Customer customer) {
		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.delete(customer);
			transaction.commit();
			session.close();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
		}
	}
}