package org.example.warehousemanagementhibernate.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.example.warehousemanagementhibernate.mapper.OrderMapper;
import org.example.warehousemanagementhibernate.model.Order;
import org.example.warehousemanagementhibernate.util.ConnectionUtil;
import org.example.warehousemanagementhibernate.util.SessionFactoryUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * Operational class used to make operations on the orders Ftable from the
 * warehouse dataBase.
 * 
 * @author GeoTrif
 *
 */
public class OrderDaoImpl implements OrderDao {
	private OrderMapper orderMapper = new OrderMapper();

	/**
	 * Used to add orders to the orders table in the warehouse dataBase.
	 * 
	 * @param order
	 *            which is an object modeled by Order class.
	 */
	public void addOrder(Order order) {
		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.save(order);
			transaction.commit();
			session.close();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
		}
	}

	/**
	 * Used to get orders by order id from the orders table in the warehouse
	 * dataBase.
	 * 
	 * @param orderId
	 *            which is an integer.
	 */
	public Order getOrderById(int orderId) {
		Order order = null;

		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = session.beginTransaction();
		order = (Order) session.get(Order.class, orderId);

		System.out.println(order);
		transaction.commit();
		session.close();
		return order;

	}

	/**
	 * Used to get all orders from the orders table in the warehouse dataBase.
	 * 
	 */
	public List<Order> getAllOrders() {
		List<Order> orders = null;

		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = session.beginTransaction();
		orders = session.createQuery("from Order").list();

		for (Order order : orders) {
			System.out.println(order);
		}

		transaction.commit();
		session.close();

		return orders;
	}

	/**
	 * Used to update orders from the orders table in the warehouse dataBase.
	 * 
	 * @param order
	 *            which is an object modeled by Order class.
	 */
	public void updateOrder(Order order) {
		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.update(order);
			transaction.commit();
			session.close();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
		}
	}

	/**
	 * Used to delete orders from the orders table in the warehouse dataBase.
	 * 
	 * @param order
	 *            which is an object modeled by Order class.
	 */
	public void deleteOrder(Order order) {
		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.delete(order);
			transaction.commit();
			session.close();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
		}
	}

	/**
	 * Used to get a particular order containing the products ordered by a user.
	 * 
	 * @param customerUsername
	 *            which is the validated username that will do the query filtering.
	 */
	public void getOrderWithProducts(String customerUsername) {
		String sql = "SELECT order_name,order_date,delivery_date,orders_products_products_id FROM warehouse.orders inner join "
				+ "orders_products on order_id = orders_products_orders_id inner join "
				+ "customers on orders.customer_id = customers.customer_id where customer_user_name = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, customerUsername);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				String orderName = resultSet.getString("order_name");
				Date orderDate = resultSet.getDate("order_date");
				Date deliveryDate = resultSet.getDate("delivery_date");
				int productId = resultSet.getInt("orders_products_products_id");

				System.out.println("Order name: " + orderName);
				System.out.println("Order date: " + orderDate);
				System.out.println("Delivery date: " + deliveryDate);
				System.out.println("Product id: " + productId);
			}

			preparedStatement.close();
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}