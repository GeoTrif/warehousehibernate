package org.example.warehousemanagementhibernate.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.example.warehousemanagementhibernate.mapper.ProductMapper;
import org.example.warehousemanagementhibernate.model.Product;
import org.example.warehousemanagementhibernate.util.ConnectionUtil;
import org.example.warehousemanagementhibernate.util.SessionFactoryUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * Operational class used to make operations on the products table from the
 * warehouse dataBase.
 * 
 * @author GeoTrif
 *
 */
public class ProductDaoImpl implements ProductDao {
	private ProductMapper productMapper = new ProductMapper();

	/**
	 * Used to add products to the products table in the warehouse dataBase.
	 * 
	 * @param product
	 *            which is an object modeled by Product class.
	 */
	public void addProduct(Product product) {
		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.save(product);
			transaction.commit();
			session.close();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
		}
	}

	/**
	 * Used to get products by product id from the products table in the warehouse
	 * dataBase.
	 * 
	 * @param productId
	 *            which is an integer.
	 */
	public Product getProductById(int productId) {
		Product product = null;

		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = session.beginTransaction();
		product = (Product) session.get(Product.class, productId);

		System.out.println(product);
		transaction.commit();
		session.close();

		return product;
	}

	/**
	 * Used to get all products from the products table in the warehouse dataBase.
	 * 
	 */
	public List<Product> getAllProducts() {
		List<Product> products = null;

		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = session.beginTransaction();
		products = session.createQuery("from Product").list();

		for (Product product : products) {
			System.out.println(product);
		}

		transaction.commit();
		session.close();
		return products;
	}

	/**
	 * Used to update proucts from the products table in the warehouse dataBase.
	 * 
	 * @param product
	 *            which is an object modeled by Product class.
	 */
	public void updateProduct(Product product) {
		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.update(product);
			transaction.commit();
			session.close();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
		}
	}

	/**
	 * Used to delete products from the products table in the warehouse dataBase.
	 * 
	 * @param product
	 *            which is an object modeled by Product class.
	 */
	public void deleteProduct(Product product) {
		Session session = SessionFactoryUtil.makeSession().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.delete(product);
			transaction.commit();
			session.close();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
		}
	}

	/**
	 * Used to get all the products from an order by using the orders id.
	 * 
	 * @param customerUsername
	 *            which is the validated username that will do the query filtering.
	 */
	public void getProductsByOrderId(String customerUsername) {
		String sql = "SELECT product_name,product_price,orders_products_orders_id FROM warehouse.products inner join "
				+ "orders_products on product_id = orders_products_products_id inner join "
				+ "orders on orders_products_orders_id = orders.order_id inner join "
				+ "customers on orders.customer_id = customers.customer_id where customer_user_name = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, customerUsername);
			ResultSet resultSet = preparedStatement.executeQuery();
			double totalPrice = 0;

			while (resultSet.next()) {
				String productName = resultSet.getString("product_name");
				double productPrice = resultSet.getDouble("product_price");
				int id = resultSet.getInt("orders_products_orders_id");
				totalPrice += productPrice;

				System.out.println("Product name: " + productName);
				System.out.println("Product price: " + productPrice);
				System.out.println("Order id: " + id);
			}

			System.out.println("Total price: " + totalPrice);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}