package org.example.warehousemanagementhibernate.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.example.warehousemanagementhibernate.model.Admin;

/**
 * Mapper class used to get the admins data from the database as a
 * collection(list).
 * 
 * @author GeoTrif
 *
 */
public class AdminMapper {

	public List<Admin> mapResultSetToAdmin(ResultSet resultSet) {
		List<Admin> admins = new ArrayList<>();

		try {
			while (resultSet.next()) {
				Admin admin = new Admin();
				admin.setAdminId(resultSet.getInt("admin_id"));
				admin.setFirstName(resultSet.getString("first_name"));
				admin.setLastName(resultSet.getString("last_name"));
				admin.setUserName(resultSet.getString("user_name"));
				admin.setPassword(resultSet.getString("password"));
				admin.setEmail(resultSet.getString("email"));
				admin.setPhoneNumber(resultSet.getString("phone_number"));
				admins.add(admin);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return admins;
	}
}
