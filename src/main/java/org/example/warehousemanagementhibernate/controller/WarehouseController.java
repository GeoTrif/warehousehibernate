package org.example.warehousemanagementhibernate.controller;

import java.io.IOException;

import org.example.warehousemanagementhibernate.service.AdminServiceImpl;
import org.example.warehousemanagementhibernate.service.CustomerServiceImpl;
import org.example.warehousemanagementhibernate.service.ProductServiceImpl;
import org.example.warehousemanagementhibernate.util.AdminPasswordValidator;
import org.example.warehousemanagementhibernate.util.AdminUsernameValidator;
import org.example.warehousemanagementhibernate.util.CustomerPasswordValidator;
import org.example.warehousemanagementhibernate.util.CustomerUsernameValidator;
import org.example.warehousemanagementhibernate.util.Menu;
import org.example.warehousemanagementhibernate.util.ScannerUtil;

/**
 * Main Controller Class used to make operations on the warehouse.
 * 
 * @author GeoTrif
 *
 */
public class WarehouseController {
	private ScannerUtil scanner = new ScannerUtil();
	private Menu menu = new Menu();
	private boolean flag = false;
	private ProductServiceImpl productService = new ProductServiceImpl();
	private AdminUsernameValidator adminUsernameValidator = new AdminUsernameValidator();
	private AdminPasswordValidator adminPasswordValidator = new AdminPasswordValidator();
	private CustomerUsernameValidator customerUsernameValidator = new CustomerUsernameValidator();
	private CustomerPasswordValidator customerPasswordValidator = new CustomerPasswordValidator();
	private AdminServiceImpl adminService = new AdminServiceImpl();
	private CustomerServiceImpl customerService = new CustomerServiceImpl();

	/**
	 * Used to provide functionality to the main menu.
	 * 
	 * @throws IOException
	 */
	public void mainMenuFunctionality() throws IOException {
		menu.printMainMenu();
		System.out.println("Enter your choice:");
		int choice = scanner.integerScanner();

		while (flag != true) {

			switch (choice) {
			case 1:
				loginFunctionality();
				break;

			case 2:
				createNewAccountMenuFunctionality();
				break;

			case 3:
				productCalatogueFunctionality();
				break;

			case 4:
				mainMenuFunctionality();
				break;

			case 5:
				System.out.println("Closing application...");
				flag = true;
				break;

			default:
				System.out.println("Please enter a valid choice.");
			}
		}
	}

	/**
	 * Used to provide functionality to the login menu.
	 * 
	 * @throws IOException
	 */
	private void loginFunctionality() throws IOException {
		menu.loginMenu();
		System.out.println("Enter your choice:");
		int choice = scanner.integerScanner();

		while (flag != true) {

			switch (choice) {
			case 1:
				System.out.println("Enter user name:");
				String adminUserName = scanner.stringScanner();
				System.out.println("Enter password:");
				String adminPassword = scanner.stringScanner();

				if (adminUsernameValidator.validateAdminUserName(adminUserName)
						&& adminPasswordValidator.validateAdminPassword(adminPassword)) {
					System.out.println("Welcome " + adminUserName + " .");
					AdminController.adminUserFunctionality();

				} else {
					System.out.println("Invalid username or password.");
				}
				break;

			case 2:
				System.out.println("Enter user name:");
				String customerUserName = scanner.stringScanner();
				System.out.println("Enter password:");
				String customerPassword = scanner.stringScanner();

				if (customerUsernameValidator.validateCustomerUserName(customerUserName)
						&& customerPasswordValidator.validateCustomerPassword(customerPassword)) {
					System.out.println("Welcome " + customerUserName + " .");
					CustomerController.customerMenuFunctionality(customerUserName);

				} else {
					System.out.println("Invalid username or password.");
				}
				break;

			case 3:
				loginFunctionality();
				break;

			case 4:
				mainMenuFunctionality();
				break;

			case 5:
				System.out.println("Closing application...");
				flag = true;
				break;

			default:
				System.out.println("Please enter a valid choice.");
			}
		}
	}

	/**
	 * Used to provide functionality to the create new account menu.
	 * 
	 * @throws IOException
	 */
	private void createNewAccountMenuFunctionality() throws IOException {
		menu.createNewAccountMenu();
		System.out.println("Enter your choice:");
		int choice = scanner.integerScanner();

		while (flag != true) {

			switch (choice) {
			case 1:
				adminService.addAdminService();
				System.out.println("Admin account created");
				createNewAccountMenuFunctionality();
				break;

			case 2:
				customerService.addCustomerService();
				System.out.println("Customer account created");
				createNewAccountMenuFunctionality();
				break;

			case 3:
				createNewAccountMenuFunctionality();
				break;

			case 4:
				mainMenuFunctionality();
				break;

			case 5:
				System.out.println("Closing application...");
				flag = true;
				break;

			default:
				System.out.println("Please enter a valid choice.");
			}
		}
	}

	/**
	 * Used to provide functionality to the product catalogue menu.
	 * 
	 * @throws IOException
	 */
	private void productCalatogueFunctionality() throws IOException {
		menu.productCatalogueMenu();
		System.out.println("Enter your choice:");
		int choice = scanner.integerScanner();

		while (flag != true) {

			switch (choice) {
			case 1:
				System.out.println("Products available in warehouse:");
				productService.getAllProductsService();
				productCalatogueFunctionality();
				break;

			case 2:
				productCalatogueFunctionality();
				break;

			case 3:
				mainMenuFunctionality();
				break;

			case 4:
				System.out.println("Closing application...");
				flag = true;
				break;

			default:
				System.out.println("Please enter a valid choice.");
			}
		}
	}
}
