package org.example.warehousemanagementhibernate.reporting;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.example.warehousemanagementhibernate.util.ConnectionUtil;
import org.example.warehousemanagementhibernate.util.ScannerUtil;

import com.itextpdf.text.DocumentException;

import jxl.write.WriteException;

/**
 * Utility class used to create a Text File Orders Reporting that includes
 * orders and products data for a particular customer.Only for customer users.
 * 
 * @author GeoTrif
 *
 */
public class CustomerFileOrdersDocument {
	private ScannerUtil scanner = new ScannerUtil();

	/**
	 * Used to create the file on the hardDisk and to write the data in the file.
	 * 
	 * @throws IOException
	 * @throws WriteException
	 */
	public void makeFileReport(String customerUsername) throws IOException {
		File file = createFile();

		writeInData(file, customerUsername + " Orders");
		writeInData(file, "Order name   " + " Order date       " + " Delivery Date        " + " Product Id           ");
		addCustomerOrderDetails(customerUsername, file);
		addEmptyLine(file);
		writeInData(file, customerUsername + " Products");
		writeInData(file, "Product name   " + " Product price       " + " Order id        ");
		addCustomerProductsByOrderDetails(customerUsername, file);
	}

	/**
	 * Used to create the text file.
	 * 
	 * @return the file that will be created on the hardDisk.
	 */
	private File createFile() {
		System.out.println("Enter file name:");
		String fileName = scanner.stringScanner();
		File file = new File("");
		file = new File(
				"C:\\Users\\GeoTrif\\Desktop\\OOP Projects\\WarehouseManagement\\Reportings\\" + fileName + ".txt");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				System.out.println("Invalid file.Please try again.");
			}
		} else {
			System.out.println("File allready exists.");
		}

		return file;
	}

	/**
	 * Used to write the data in the text file.
	 * 
	 * @param file
	 *            which will be the file to write the data.
	 * @param content
	 *            which will be the data to be written in the text file.
	 * @throws IOException
	 */
	private void writeInData(File file, String content) throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true));

		try {
			bufferedWriter.write(content);
			bufferedWriter.newLine();
			bufferedWriter.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			if (bufferedWriter != null) {
				bufferedWriter.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to add orders data to the file.
	 * 
	 * @param customerUsername
	 *            which is the username used to login.
	 * @param file
	 *            which will be the file to write the data.
	 * @throws DocumentException
	 */
	private void addCustomerOrderDetails(String customerUsername, File file) throws IOException {
		String sql = "SELECT order_name,order_date,delivery_date,orders_products_products_id FROM warehouse.orders inner join "
				+ "orders_products on order_id = orders_products_orders_id inner join "
				+ "customers on orders.customer_id = customers.customer_id where customer_user_name = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, customerUsername);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				String orderName = resultSet.getString("order_name");
				Date orderDate = resultSet.getDate("order_date");
				Date deliveryDate = resultSet.getDate("delivery_date");
				int productId = resultSet.getInt("orders_products_products_id");

				writeInData(file, orderName + "  " + orderName.toString() + "   " + deliveryDate.toString() + "    "
						+ Integer.toString(productId));

			}

			preparedStatement.close();
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to add products data to the file.
	 * 
	 * @param customerUsername
	 *            which is the username used to login.
	 * @param file
	 *            which will be the file to write the data.
	 * @throws DocumentException
	 */
	private void addCustomerProductsByOrderDetails(String customerUsername, File file) throws IOException {
		String sql = "SELECT product_name,product_price,orders_products_orders_id FROM warehouse.products inner join "
				+ "orders_products on product_id = orders_products_products_id inner join "
				+ "orders on orders_products_orders_id = orders.order_id inner join "
				+ "customers on orders.customer_id = customers.customer_id where customer_user_name = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, customerUsername);
			ResultSet resultSet = preparedStatement.executeQuery();
			double totalPrice = 0;

			while (resultSet.next()) {
				String productName = resultSet.getString("product_name");
				double productPrice = resultSet.getDouble("product_price");
				int id = resultSet.getInt("orders_products_orders_id");
				totalPrice += productPrice;

				writeInData(file, productName + "   " + Double.toString(productPrice) + "   " + Integer.toString(id));

			}

			writeInData(file, "Total price" + Double.toString(totalPrice));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to add an empty space between data.
	 * 
	 * @param file
	 *            which will be the file to write the data.
	 * @throws IOException
	 */
	private void addEmptyLine(File file) throws IOException {
		writeInData(file, "");
	}

}
