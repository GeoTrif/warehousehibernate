package org.example.warehousemanagementhibernate.reporting;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.example.warehousemanagementhibernate.util.ConnectionUtil;
import org.example.warehousemanagementhibernate.util.ScannerUtil;

import jxl.write.WriteException;

/**
 * Utility class used to create a Text File General Reporting that includes all
 * the data from the database.Only for admin users.
 * 
 * @author GeoTrif
 *
 */
public class AdminFileGeneralReport {
	private ScannerUtil scanner = new ScannerUtil();

	/**
	 * Used to create the file on the hardDisk and to write the data in the file.
	 * 
	 * @throws IOException
	 * @throws WriteException
	 */
	public void makeFileReport() throws IOException {
		File file = createFile();

		getAdminsDetails(file);
		addEmptyLine(file);
		getCustomersDetails(file);
		addEmptyLine(file);
		getOrdersDetails(file);
		addEmptyLine(file);
		getProductsDetails(file);
	}

	/**
	 * Used to create the text file.
	 * 
	 * @return the file that will be created on the hardDisk.
	 */
	private File createFile() {
		System.out.println("Enter file name:");
		String fileName = scanner.stringScanner();
		File file = new File("");
		file = new File(
				"C:\\Users\\GeoTrif\\Desktop\\OOP Projects\\WarehouseManagement\\Reportings\\" + fileName + ".txt");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				System.out.println("Invalid file.Please try again.");
			}
		} else {
			System.out.println("File allready exists.");
		}

		return file;

	}

	/**
	 * Used to write the data in the text file.
	 * 
	 * @param file
	 *            which will be the file to write the data.
	 * @param content
	 *            which will be the data to be written in the text file.
	 * @throws IOException
	 */
	private void writeInData(File file, String content) throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true));

		try {
			bufferedWriter.write(content);
			bufferedWriter.newLine();
			bufferedWriter.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			if (bufferedWriter != null) {
				bufferedWriter.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to get the admins data from the database.
	 * 
	 * @param file
	 *            which will be the file to write the data.
	 * @throws IOException
	 */
	private void getAdminsDetails(File file) throws IOException {

		String sql = "select * from admins";
		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				int id = resultSet.getInt("admin_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				String userName = resultSet.getString("user_name");
				String password = resultSet.getString("password");
				String email = resultSet.getString("email");
				String phoneNumber = resultSet.getString("phone_number");

				writeInData(file, "Admin details:");
				writeInData(file, "\tAdmin Id    " + "First Name    " + "Last Name    " + "User Name    "
						+ "Password    " + "Email    " + "Phone Number    ");
				writeInData(file, "\t" + Integer.toString(id) + "    " + firstName + "    " + lastName + "    "
						+ userName + "    " + password + "    " + email + "    " + phoneNumber);
			}

			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to get the customers data from the database.
	 * 
	 * @param file
	 *            which will be the file to write the data.
	 * @throws IOException
	 */
	private void getCustomersDetails(File file) throws IOException {
		String sql = "select * from customers";
		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				int id = resultSet.getInt("customer_id");
				String firstName = resultSet.getString("customer_first_name");
				String lastName = resultSet.getString("customer_last_name");
				String userName = resultSet.getString("customer_user_name");
				String password = resultSet.getString("customer_password");
				String email = resultSet.getString("customer_email");
				String phoneNumber = resultSet.getString("customer_phone_number");

				writeInData(file, "Customers details:");
				writeInData(file, "\tCustomer Id    " + "First Name    " + "Last Name    " + "User Name    "
						+ "Password    " + "Email    " + "Phone Number    ");
				writeInData(file, "\t" + Integer.toString(id) + "    " + firstName + "    " + lastName + "    "
						+ userName + "    " + password + "    " + email + "    " + phoneNumber);
			}

			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to get the orders data from the database.
	 * 
	 * @param file
	 *            which will be the file to write the data.
	 * @throws IOException
	 */
	private void getOrdersDetails(File file) throws IOException {
		String sql = "select * from orders";

		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				int id = resultSet.getInt("order_id");
				String orderName = resultSet.getString("order_name");
				Date orderDate = resultSet.getDate("order_date");
				Date deliveryDate = resultSet.getDate("delivery_date");
				int customerId = resultSet.getInt("customer_id");

				writeInData(file, "Orders details:");
				writeInData(file, "\tOrder Id    " + "Order Name    " + "Order Date    " + "Delivery Date    "
						+ "Customer Id    ");
				writeInData(file, "\t" + id + "    " + orderName + "    " + orderDate + "    " + deliveryDate + "    "
						+ customerId);
			}

			statement.close();
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to get the products data from the database.
	 * 
	 * @param file
	 *            which will be the file to write the data.
	 * @throws IOException
	 */
	private void getProductsDetails(File file) throws IOException {
		String sql = "select * from products";

		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				int id = resultSet.getInt("product_id");
				String productName = resultSet.getString("product_name");
				double productPrice = resultSet.getDouble("product_price");

				writeInData(file, "Products details:");
				writeInData(file, "\tProduct Id    " + "Product Name    " + "Product Price    ");
				writeInData(file, "\t" + id + "    " + productName + "    " + productPrice);
			}

			statement.close();
			resultSet.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to add an empty space between data.
	 * 
	 * @param file
	 *            which will be the file to write the data.
	 * @throws IOException
	 */
	private void addEmptyLine(File file) throws IOException {
		writeInData(file, "");
	}
}
