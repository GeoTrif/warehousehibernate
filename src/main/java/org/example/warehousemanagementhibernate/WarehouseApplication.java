package org.example.warehousemanagementhibernate;

import java.io.IOException;

import org.example.warehousemanagementhibernate.controller.WarehouseController;

/**
 * Driver Class used to start the application.
 * 
 * @author GeoTrif
 *
 */
public class WarehouseApplication {

	/**
	 * Driver method.
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		WarehouseController controller = new WarehouseController();
		controller.mainMenuFunctionality();
	}
}
