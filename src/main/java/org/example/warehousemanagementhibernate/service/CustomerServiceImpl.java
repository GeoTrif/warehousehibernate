package org.example.warehousemanagementhibernate.service;

import java.util.List;

import org.example.warehousemanagementhibernate.dao.CustomerDaoImpl;
import org.example.warehousemanagementhibernate.model.Customer;
import org.example.warehousemanagementhibernate.util.ScannerUtil;

/**
 * Service class used to interact with the user input in order to do operations
 * on the customers table from the data base.
 * 
 * @author GeoTrif
 *
 */
public class CustomerServiceImpl {
	private ScannerUtil scanner = new ScannerUtil();
	private CustomerDaoImpl customerDaoImpl = new CustomerDaoImpl();

	public void addCustomerService() {
		System.out.println("Enter first name:");
		String firstName = scanner.stringScanner();
		System.out.println("Enter last name:");
		String lastName = scanner.stringScanner();
		System.out.println("Enter username:");
		String userName = scanner.stringScanner();
		System.out.println("Enter password:");
		String password = scanner.stringScanner();
		System.out.println("Enter email:");
		String email = scanner.stringScanner();
		System.out.println("Enter phone number:");
		String phoneNumber = scanner.stringScanner();

		Customer customer = new Customer(firstName, lastName, userName, password, email, phoneNumber);
		customerDaoImpl.addCustomer(customer);
	}

	public void getCustomerByIdService() {
		System.out.println("Enter customer id:");
		int id = scanner.integerScanner();

		Customer customer = new Customer(id);
		customerDaoImpl.getCustomerById(id);
	}

	public List<Customer> getAllCustomersService() {
		return customerDaoImpl.getAllCustomers();
	}

	public void updateCustomerService() {
		System.out.println("Enter customer id:");
		int id = scanner.integerScanner();
		System.out.println("Enter first name:");
		String firstName = scanner.stringScanner();
		System.out.println("Enter last name:");
		String lastName = scanner.stringScanner();
		System.out.println("Enter user name:");
		String userName = scanner.stringScanner();
		System.out.println("Enter password:");
		String password = scanner.stringScanner();
		System.out.println("Enter email:");
		String email = scanner.stringScanner();
		System.out.println("Enter phone number:");
		String phoneNumber = scanner.stringScanner();

		Customer customer = new Customer(id, firstName, lastName, userName, password, email, phoneNumber);
		customerDaoImpl.updateCustomer(customer);
	}

	public void deleteCustomerService() {
		System.out.println("Enter customer id:");
		int id = scanner.integerScanner();

		Customer customer = new Customer(id);
		customerDaoImpl.deleteCustomer(customer);
	}
}
