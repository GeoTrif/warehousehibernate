package org.example.warehousemanagementhibernate.service;

import org.example.warehousemanagementhibernate.dao.ProductDaoImpl;
import org.example.warehousemanagementhibernate.model.Product;
import org.example.warehousemanagementhibernate.util.ScannerUtil;

/**
 * Service class used to interact with the user input in order to do operations
 * on the products table from the data base.
 * 
 * @author GeoTrif
 *
 */
public class ProductServiceImpl {
	private ScannerUtil scanner = new ScannerUtil();
	private ProductDaoImpl productDaoImpl = new ProductDaoImpl();

	public void addAProductService() {
		System.out.println("Enter product name:");
		String productName = scanner.stringScanner();
		System.out.println("Enter product price:");
		double productPrice = scanner.doubleScanner();

		Product product = new Product(productName, productPrice);
		productDaoImpl.addProduct(product);
	}

	public void getProductByIdService() {
		System.out.println("Enter product id:");
		int id = scanner.integerScanner();
		productDaoImpl.getProductById(id);
	}

	public void getAllProductsService() {
		productDaoImpl.getAllProducts();
	}

	public void updateProductService() {
		System.out.println("Enter product id:");
		int id = scanner.integerScanner();
		System.out.println("Enter product name:");
		String productName = scanner.stringScanner();
		System.out.println("Enter product price:");
		double productPrice = scanner.doubleScanner();

		Product product = new Product(id, productName, productPrice);
		productDaoImpl.updateProduct(product);
	}

	public void deleteProductService() {
		System.out.println("Enter product id:");
		int id = scanner.integerScanner();

		Product product = new Product(id);
		productDaoImpl.deleteProduct(product);
	}

	public void getProductsByOrderIdService(String customerUsername) {
		productDaoImpl.getProductsByOrderId(customerUsername);
	}
}
