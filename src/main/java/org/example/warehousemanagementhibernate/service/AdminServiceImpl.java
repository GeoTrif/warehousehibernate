package org.example.warehousemanagementhibernate.service;

import org.example.warehousemanagementhibernate.dao.AdminDaoImpl;
import org.example.warehousemanagementhibernate.model.Admin;
import org.example.warehousemanagementhibernate.util.ScannerUtil;

/**
 * Service class used to interact with the user input in order to do operations
 * on the admins table from the data base.
 * 
 * @author GeoTrif
 *
 */
public class AdminServiceImpl {
	private ScannerUtil scanner = new ScannerUtil();
	private AdminDaoImpl adminDaoImpl = new AdminDaoImpl();

	public void addAdminService() {
		System.out.println("Enter First Name: ");
		String firstName = scanner.stringScanner();
		System.out.println("Enter Last Name: ");
		String lastName = scanner.stringScanner();
		System.out.println("Enter UserName: ");
		String userName = scanner.stringScanner();
		System.out.println("Enter Password: ");
		String password = scanner.stringScanner();
		System.out.println("Enter Email: ");
		String email = scanner.stringScanner();
		System.out.println("Enter Phone Number: ");
		String phoneNumber = scanner.stringScanner();

		Admin admin = new Admin(firstName, lastName, userName, password, email, phoneNumber);
		adminDaoImpl.addAdmin(admin);
	}

	public void getAdminByIdService() {
		System.out.println("Enter admin id: ");
		int id = scanner.integerScanner();
		adminDaoImpl.getAdminById(id);
	}

	public void getAllAdminsService() {
		adminDaoImpl.getAllAdmins();
	}

	public void updateAdminService() {
		System.out.println("Enter Admin Id:");
		int id = scanner.integerScanner();
		System.out.println("Enter First Name: ");
		String firstName = scanner.stringScanner();
		System.out.println("Enter Last Name: ");
		String lastName = scanner.stringScanner();
		System.out.println("Enter UserName: ");
		String userName = scanner.stringScanner();
		System.out.println("Enter Password: ");
		String password = scanner.stringScanner();
		System.out.println("Enter Email: ");
		String email = scanner.stringScanner();
		System.out.println("Enter Phone Number: ");
		String phoneNumber = scanner.stringScanner();

		Admin admin = new Admin(id, firstName, lastName, userName, password, email, phoneNumber);
		adminDaoImpl.updateAdmin(admin);
	}

	public void deleteAdminService() {
		System.out.println("Enter admin id: ");
		int id = scanner.integerScanner();

		Admin admin = new Admin(id);
		adminDaoImpl.deleteAdmin(admin);
	}
}
